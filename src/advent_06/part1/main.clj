(ns advent-06.part1.main)

(def filename "inputs/06.txt")

(defn- count-orbital-distance
  [orbital-lookup outer]
  (loop [orbit-count 0
         current-outer outer]
    (if (contains? orbital-lookup current-outer)
      (recur (+ orbit-count 1) (get orbital-lookup current-outer))
      orbit-count)))

; Outputs 171213
(defn run-part!
  []
  (let [orbits (->> filename
                    slurp
                    clojure.string/split-lines
                    (map #(clojure.string/split % #"\)"))
                    (reduce (fn [iter [inner outer]]
                              (assoc iter outer inner))
                            {}))
        orbital-distance-counter (partial count-orbital-distance orbits)
        orbital-checksum (->> orbits
                              keys
                              (map orbital-distance-counter)
                              (apply +))]
    (println "Day 6 - Part 1:" orbital-checksum)))
