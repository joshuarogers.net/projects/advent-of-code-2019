(ns advent-06.part2.main)

(def filename "inputs/06.txt")

(defn- generate-orbital-chain
  [orbital-lookup outer]
  (loop [orbital-chain []
         current-outer outer]
    (if (contains? orbital-lookup current-outer)
      (recur (conj orbital-chain current-outer) (get orbital-lookup current-outer))
      orbital-chain)))

; Outputs 292
(defn run-part!
  []
  (let [orbits (->> filename
                    slurp
                    clojure.string/split-lines
                    (map #(clojure.string/split % #"\)"))
                    (reduce (fn [iter [inner outer]]
                              (assoc iter outer inner))
                            {}))
        san-orbital-chain (set (rest (generate-orbital-chain orbits "SAN")))
        you-orbital-chain (set (rest (generate-orbital-chain orbits "YOU")))
        common-bodies (clojure.set/intersection san-orbital-chain you-orbital-chain)
        all-bodies (clojure.set/union san-orbital-chain you-orbital-chain)
        unique-bodies (clojure.set/difference all-bodies common-bodies)]
    ; Takes the logic that, given two orbital-chains (the ordered list of all
    ; bodies a body directly or indirectly orbits), the count of moves needed
    ; will be the number of unique bodies, excluding the two origins.
    (println "Day 6 - Part 2:" (count unique-bodies))))
