(ns advent-01.part2
  (:require [advent-01.part1 :as part1]))

(defn calculate-fuel-for-component
  ([uncalculated-fuel-mass] (calculate-fuel-for-component uncalculated-fuel-mass 0))
  ([uncalculated-fuel-mass previous-calculated-total]
   (if (<= uncalculated-fuel-mass 0)
     previous-calculated-total
     (let [additional-fuel-required (part1/calculate-fuel-requirement uncalculated-fuel-mass)
           calculated-total (+ previous-calculated-total additional-fuel-required)]
       (recur additional-fuel-required calculated-total)))))

(defn calculate-fuel-for-ship-mass!
  []
  (->> (part1/read-masses part1/masses-filename)
       (map calculate-fuel-for-component)
       (apply +')))

; Outputs 4870838
(defn run-part!
  []
  (let [total-fuel-mass (calculate-fuel-for-ship-mass!)]
    (println "Day 01 - Part 2:" total-fuel-mass)))