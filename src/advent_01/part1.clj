(ns advent-01.part1)

(def masses-filename "inputs/01.txt")

(defn parse-int
  [x]
  (Integer/parseInt x))

(defn round-down
  [x]
  (Math/floor x))

(defn read-masses
  [input-filename]
  (->> (slurp input-filename)
       (clojure.string/split-lines)
       (map parse-int)))

(defn calculate-fuel-requirement
  [mass]
  (max 0 (-> mass
       (/ 3)
       (round-down)
       (- 2)
       (int))))

(defn calculate-fuel-for-ship-mass!
  []
  (->> (read-masses masses-filename)
       (map calculate-fuel-requirement)
       (apply +')))

; Outputs 3249140
(defn run-part!
  []
  (let [total-fuel (calculate-fuel-for-ship-mass!)]
    (println "Day 01 - Part 1:" total-fuel)))
