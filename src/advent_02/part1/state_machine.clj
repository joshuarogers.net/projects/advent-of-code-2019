(ns advent-02.part1.state-machine)

(defn ->intcode-state-machine
  ([memory] (->intcode-state-machine memory 0))
  ([memory instruction-counter]
   { :memory memory
     :instruction-counter instruction-counter
     :state :running }))

(defn intcode-at
  [{memory :memory} index]
  (memory index))

(defn- intcode-at-offset
  [{instruction-counter :instruction-counter :as state-machine} offset]
  (let [offset-intcode-index (+ instruction-counter offset)]
    (intcode-at state-machine offset-intcode-index)))

(defn- next-intcode
  [state-machine]
  (intcode-at-offset state-machine 0))

(defn update-intcode-at
  [state-machine index value]
  (assoc-in state-machine [:memory index] value))

(defn- advance-instruction-counter
  [{instruction-counter :instruction-counter :as state-machine} instruction-count]
  (assoc state-machine :instruction-counter (+ instruction-counter instruction-count)))

(defn- halt-and-catch-fire
  [state-machine]
  (assoc state-machine :state :halted))

; The step multimethod chooses which implementation to call based on the next opcode in memory
(defmulti step next-intcode)

(defn run
  [state-machine]
  (if (= (:state state-machine) :running)
    (recur (step state-machine))
    state-machine))

; ADD instruction - "1 x y target-index"
(defmethod step 1
  [state-machine]
  (let [param (fn [x] (intcode-at-offset state-machine x))
        param-deref (fn [x] (intcode-at state-machine (param x)))
        x (param-deref 1)
        y (param-deref 2)
        target-index (param 3)
        result (+ x y)]
    (-> state-machine
        (update-intcode-at target-index result)
        (advance-instruction-counter 4))))

; MUL instruction - "2 x y target-index"
(defmethod step 2
  [state-machine]
  (let [param (fn [x] (intcode-at-offset state-machine x))
        param-deref (fn [x] (intcode-at state-machine (param x)))
        x (param-deref 1)
        y (param-deref 2)
        target-index (param 3)
        result (* x y)]
    (-> state-machine
        (update-intcode-at target-index result)
        (advance-instruction-counter 4))))

; HCL instruction - "99"
(defmethod step 99
  [state-machine]
  (halt-and-catch-fire state-machine))