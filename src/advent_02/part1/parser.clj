(ns advent-02.part1.parser)

(defn parse-int
  [x]
  (Integer/parseInt x))

(defn parse-intcodes
  [s]
  (->> (clojure.string/split s #",")
       (map parse-int)))
