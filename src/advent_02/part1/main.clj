(ns advent-02.part1.main
  (:require [advent-02.part1.parser :refer [parse-intcodes]]
           [advent-02.part1.state-machine :as state-machine]))

(def intcode-memory-filename "inputs/02.txt")

; Outputs 6730673
(defn run-part!
  []
  (let [initial-state-machine (-> intcode-memory-filename
                     slurp
                     parse-intcodes
                     vec
                     state-machine/->intcode-state-machine
                     (state-machine/update-intcode-at 1 12)
                     (state-machine/update-intcode-at 2 2))
        completed-state-machine (state-machine/run initial-state-machine)]
    (println "Day 2 - Part 1:" (state-machine/intcode-at completed-state-machine 0))))
