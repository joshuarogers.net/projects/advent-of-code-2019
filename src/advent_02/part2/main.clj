(ns advent-02.part2.main
  (:require [advent-02.part1.parser :refer [parse-intcodes]]
            [advent-02.part1.state-machine :as state-machine]))

(def intcode-memory-filename "inputs/02.txt")

; Outputs 3749
(defn run-part!
  []
  (let [target-state 19690720
        initial-state-machine (-> intcode-memory-filename
                                  slurp
                                  parse-intcodes
                                  vec
                                  state-machine/->intcode-state-machine)
        potential-patched-states (for [x (range 100)
                                       y (range 100)]
                                   (-> initial-state-machine
                                       (state-machine/update-intcode-at 1 x)
                                       (state-machine/update-intcode-at 2 y)))
        target (->> potential-patched-states
                    (filter #(= (state-machine/intcode-at (state-machine/run %) 0) target-state))
                    first)]
    (println "Day 2 - Part 2:" (format "%02d%02d"
                                       (state-machine/intcode-at target 1)
                                       (state-machine/intcode-at target 2)))))
