(ns core
  (:require [advent-01.part1 :as advent-01-part1]
            [advent-01.part2 :as advent-01-part2]
            [advent-02.part1.main :as advent-02-part1]
            [advent-02.part2.main :as advent-02-part2]
            [advent-03.part1.main :as advent-03-part1]
            [advent-03.part2.main :as advent-03-part2]
            [advent-04.part1.main :as advent-04-part1]
            [advent-04.part2.main :as advent-04-part2]
            [advent-05.part1.main :as advent-05-part1]
            [advent-05.part2.main :as advent-05-part2]
            [advent-06.part1.main :as advent-06-part1]
            [advent-06.part2.main :as advent-06-part2]
            [advent-07.part1.main :as advent-07-part1])
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (do
    '(advent-01-part1/run-part!)
    '(advent-01-part2/run-part!)
    '(advent-02-part1/run-part!)
    '(advent-02-part2/run-part!)
    '(advent-03-part1/run-part!)
    '(advent-03-part2/run-part!)
    '(advent-04-part1/run-part!)
    '(advent-04-part2/run-part!)
    '(advent-05-part1/run-part!)
    '(advent-05-part2/run-part!)
    '(advent-06-part1/run-part!)
    '(advent-06-part2/run-part!)
    (advent-07-part1/run-part!)))
