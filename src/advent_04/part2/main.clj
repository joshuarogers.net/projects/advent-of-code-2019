(ns advent-04.part2.main)

(def min-value 284639)
(def max-value 748759)

(defn parse-int
  [x]
  (Integer/parseInt x))

(defn- password-filter
  [numeric-value]
  (let [password-characters (->> (clojure.string/split (str numeric-value) #"")
                                 (map parse-int))
        password-character-neighbor-pairs (map list
                                               password-characters
                                               (rest password-characters))
        password-character-repetition-counts (map #(-> %
                                                       second
                                                       count)
                                                  (group-by identity password-characters))]
    (and
      (every? (fn [[x y]] (<= x y)) password-character-neighbor-pairs)
      (some (fn [x] (= x 2)) password-character-repetition-counts))))

; Outputs 591
(defn run-part!
  []
  (let [unfiltered-range (range min-value (+ max-value 1))
        filtered-range (filter password-filter unfiltered-range)]
    (println "Day 4 - Part 2:" (count filtered-range))))
