(ns advent-05.part2.main
  (:require [advent-05.part2.parser :refer [parse-intcodes]]
            [advent-05.part2.state-machine :as state-machine]))

(def intcode-memory-filename "inputs/05.txt")

; Outputs 15426686
(defn run-part!
  []
  (let [initial-state-machine (-> intcode-memory-filename
                     slurp
                     parse-intcodes
                     vec
                     state-machine/->intcode-state-machine
                     (state-machine/buffer-input 5))
        completed-state-machine (state-machine/run initial-state-machine)]
    (println "Day 5 - Part 2:" (last (:output-buffer completed-state-machine)))))
