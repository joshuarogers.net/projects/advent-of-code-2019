(ns advent-05.part2.parser)

(defn parse-int
  [x]
  (Integer/parseInt x))

(defn parse-intcodes
  [s]
  (->> (clojure.string/split s #",")
       (map parse-int)))
